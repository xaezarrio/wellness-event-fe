// eslint-disable-next-line import/no-anonymous-default-export
export default {
	getUserToken: () => {
		const userToken = localStorage.getItem("userToken");
		return userToken;
	},
	setUserToken: token => {
		localStorage.setItem("userToken", token);
	},
	removeToken: () => {
		localStorage.clear();
	}
};
