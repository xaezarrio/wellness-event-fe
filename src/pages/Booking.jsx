import React from 'react'
import { Layout, Button, Breadcrumb, Typography } from 'antd';
import BookingTable from '../modules/Booking/BookingTable';
import { BookingProvider } from '../modules/Booking/BookingContext';
import BookingModalAdd from '../modules/Booking/BookingModalAdd';
import BookingModalEdit from '../modules/Booking/BookingModalEdit';
import auth from '../apis/auth'
import { useDispatch } from "react-redux";
import { logout } from '../store/auth/action';
import { userData } from "../helper/Common";

const { Header, Content, Footer } = Layout;

const Booking = () => {

  const dispatch = useDispatch();

  const Title = Typography

  const { name } = userData() 

  const handleLogout = () => {
      auth.logout();
      dispatch(logout());
      
      window.location.href = "/login";
  }

  return (<>
    <Layout className="layout">
      <Header>
        <Button type="primary" style={{float: 'right', marginTop: '15px'}} onClick={handleLogout}>Log Out</Button>
        <Title level={2} style={{color: 'white'}}>Hello, {name}</Title>
      </Header>
      <Content style={{ padding: '0 50px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>Booking</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item> 
        </Breadcrumb>
        <div className="site-layout-content">
          <BookingProvider>
            <BookingTable />
            <BookingModalAdd />
            <BookingModalEdit />
          </BookingProvider>
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>Wellness Event Booking APPS</Footer>
    </Layout>
  </>)
}

export default Booking