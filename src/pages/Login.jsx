import React, { useState } from 'react'
import { Button, Form, Input, Row, Col, Card, Typography, Alert } from 'antd';
import auth from '../apis/auth'
import { useDispatch } from "react-redux";
import { AUTHENTICATED, AUTHENTICATION_ERROR, login } from '../store/auth/action';

const Login = () => {

  const { Title } = Typography;

  const dispatch = useDispatch();

  const [errMsg, setErrMsg] = useState('');
  const [loading, setLoading] = useState(false);

  const onFinish = async (values) => {
    setLoading(true);
    await auth.login(values)
        .then(result => {
            dispatch(login(AUTHENTICATED, result));
            window.location.href = "/";
        })
        .catch(err => {
            setErrMsg(err);
            dispatch(login(AUTHENTICATION_ERROR, err ? err : 'Internal Server Error'));
        })
    setLoading(false);
  };
  

  return (<>
          <Row type="flex" justify="center" align="middle" style={{minHeight: '100vh'}}>
            <Col>
              <Card>
                <Row type="flex" justify="center" align="middle" style={{minHeight: '10vh'}}>
                  <Col>
                    <Title level={3}>Wellness Event</Title>
                  </Col>
                </Row>
                {errMsg ? (
                  <Alert message={errMsg} type="error" showIcon closable afterClose={() => setErrMsg("")} style={{ marginBottom: 10}}/>
                ) : null}
                <Form
                  name="login"
                  labelCol={{ span: 8 }}
                  wrapperCol={{ span: 16 }}
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                  autoComplete="off"
                >
                  <Form.Item
                    label="Username"
                    name="username"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item wrapperCol={{ span: 24 }}>
                    <Button type="primary" htmlType="submit" block loading={loading}>
                      Log In
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
  </>)
}

export default Login