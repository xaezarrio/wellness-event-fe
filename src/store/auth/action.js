export const AUTHENTICATED = 'AUTHENTICATED'
export const UNAUTHENTICATED = 'UNAUTHENTICATED'
export const AUTHENTICATION_ERROR = 'AUTHENTICATION_ERROR'  

export const login = (type, payload) => {
    return {
      type,
      payload,
    };
};


export const logout = () => {
    return {
        type: UNAUTHENTICATED
    };
  }