import {
    AUTHENTICATED,
    UNAUTHENTICATED,
    AUTHENTICATION_ERROR
  } from './action'

const authReducer = (state = {}, action) => {
    switch (action.type) {
        case AUTHENTICATED:
        return {
            ...state,
            token: action.payload.token,
            authenticated: true,
        }
        case UNAUTHENTICATED:
        return {
            ...state,
            authenticated: false,
        }
        case AUTHENTICATION_ERROR:
        return {
            ...state,
            error: action.payload,
        }

        default:
        return state
    }
}

export default authReducer
  