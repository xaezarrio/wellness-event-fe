import api from './api';
import authHeader from './headers/authHeader';
const URL = "postal-code";

const getAllProvince = () => {
    return new Promise(function(resolve, reject){
        api.get(`${URL}/province`, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

const getPostalCodeByProvince = (code, postalCode) => {
    return new Promise(function(resolve, reject){
        api.get(`${URL}/${code}/${postalCode}`, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

const getAllPostal = (postal) => {
    return new Promise(function(resolve, reject){
        api.get(`${URL}/${postal}`, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};
const getAddressByPostal = (postal) => {
    return new Promise(function(resolve, reject){
        api.get(`${URL}/address/${postal}`, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getAllProvince,
    getPostalCodeByProvince,
    getAllPostal,
    getAddressByPostal
}