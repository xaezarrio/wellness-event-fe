import api from "./api";
import TokenService from "../services/token.service";

const URL = "auth";

const login = value => {
	return new Promise(function(resolve, reject) {
		api.post(`${URL}/login`, value)
			.then(result => {
				TokenService.setUserToken(result.data.accessToken);
				resolve({
					token: result.data.accessToken
				});
			})
			.catch(error => {
				if (error.response.data.message) {
					reject(error.response.data.message);
				}
				reject(error);
			});
	});
};

const refreshToken = () => {
	return new Promise(function(resolve, reject) {
		api.get(`${URL}/request-token`)
			.then(result => {
				TokenService.setUserToken(result.data.accessToken);
				resolve({
					token: result.data.accessToken,
				});
			})
			.catch(error => {
				reject(error);
			});
	});
};

const logout = () => {
	
	localStorage.clear();
	
	api.delete(`${URL}/logout`)

};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
	login,
	logout,
	refreshToken
};
