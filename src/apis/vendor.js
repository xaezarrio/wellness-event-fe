import api from './api';
import authHeader from './headers/authHeader';
const URL = "vendor";

const getAllVendor = () => {
    return new Promise(function(resolve, reject){
        api.get(`${URL}`, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getAllVendor
}