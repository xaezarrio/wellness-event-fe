import TokenService from "../../services/token.service";

export default function authHeader() {
	const userToken = TokenService.getUserToken();

	if (userToken) {
		return {
			"Content-type": "application/json",
			Authorization: `Bearer ${userToken}`
		};
	} else {
		return {};
	}
}
