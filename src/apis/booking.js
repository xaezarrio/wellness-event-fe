import api from './api';
import authHeader from './headers/authHeader';
const URL = "booking";

const getAllBookings = (values) => {
    return new Promise(function(resolve, reject){
        api.get(`${URL}?event=${values}`, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

const storeBooking = (values) => {
    return new Promise(function(resolve, reject){
        api.post(`${URL}`, values, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

const getBookingById = (id) => {
    return new Promise(function(resolve, reject){
        api.get(`${URL}/${id}`, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

const updateStatusBooking = (id, values) => {
    return new Promise(function(resolve, reject){
        api.patch(`${URL}/${id}`, values, authHeader())
            .then(result => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
				if (error.response.data.error) {
					reject(error.response.data.error);
				}
				reject(error);
            });
    });
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    storeBooking,
    getAllBookings,
    getBookingById,
    updateStatusBooking
}