import axios from "axios";
import auth from "./auth";
import TokenService from "../services/token.service";


const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
	headers: {
		"Content-Type": "application/json"
	}
});
api.defaults.withCredentials = true;


api.interceptors.request.use(config => {
	const token = TokenService.getUserToken();

	new Promise(() => {
		if (!token && config.url!=="auth/login") window.location.href = "/login";
	});

	config.headers.Authorization = `Bearer ${token}`;
	return config;
});

api.interceptors.response.use(
	response => {
		return response;
	},
	async error => {
		if (error.response.status === 401) {
			const newToken = await auth.refreshToken();
			error.config.headers["Authorization"] = `Bearer ${newToken.token}`;
			return axios(error.config);
		} else {
			console.log(error);
			return Promise.reject(error);
		}
	}
);

export default api;
