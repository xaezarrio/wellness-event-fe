import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import routes from './routes'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        {routes &&
          routes.map(item => (
            <Route
              exact={item.exact}
              path={item.path}
              key={item.name}
              element={<item.component />}
            />
          ))}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
