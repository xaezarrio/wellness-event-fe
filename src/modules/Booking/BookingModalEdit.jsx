import React, { useContext, useState } from 'react'
import { Modal, Button, Descriptions, Tag, message } from 'antd';
import { BookingContext } from './BookingContext';
import moment from 'moment';
import { ROLE } from "../../helper/Constant";
import { userData } from "../../helper/Common";
import Swal from 'sweetalert2';
import booking from '../../apis/booking';

const BookingModalEdit = () => {

  
  const {
    state: {
      showModal,
      tableParams
    },
    data: {
      userData,
      dataViewEvent
    },
    action: {
      toggleModal,
      handleFetchDataTable
    }
  } = useContext(BookingContext)

  const [loadingProcess, setLoadingProcess] = useState(false)

  let footer = [
    <Button key="back" onClick={() => toggleModal('view')}>
      Return
    </Button>
  ]

  if (userData.role === ROLE.Vendor && dataViewEvent.status === "pending"){
    footer = [...footer,
      <Button key="btnReject" danger loading={loadingProcess} onClick={() => handleRejectEvent(dataViewEvent.id)}>
        Reject
      </Button>,
      <Button
        key="btnApprove"
        type="primary"
        loading={loadingProcess}
        onClick={() => handleApproveEvent(dataViewEvent.id, dataViewEvent.purposedDate)}
      >
        Approve
      </Button>,
    ]
  }

  const handleRejectEvent = async (id) => {
    setLoadingProcess(true);
    await Swal.fire({
        title: 'Enter remark to reject the Event',
        input: 'text',
        showCancelButton: true,
        inputPlaceholder: 'Remark',
        confirmButtonColor: '#dc3545',
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit!',
        backdrop: true,
        inputValidator: (value) => {
          if (!value) {
            return 'Remark is required!'
          }
        },
        preConfirm: async (remark) => {
            return booking.updateStatusBooking(id, {status: "rejected", remark})
                .then(result => {
                    if (!result.success) {
                        Swal.showValidationMessage(
                            `Internal server error, please try again`
                        );
                    }
                    message.success(result.message);
                    handleFetchDataTable(tableParams.keyword);
                    toggleModal('view');
                })
            .catch(error => {
                Swal.showValidationMessage(
                    `Internal server error, please try again`
                );
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
      })

      setLoadingProcess(false);
  }

  const handleApproveEvent = async (id, date) => {
    setLoadingProcess(true);
    let optDate = date.reduce((prev, current) => {
        prev[current] = current;
        return prev;
      }, {});
    await Swal.fire({
        title: 'Please choose approved date',
        input: 'select',
        inputOptions: optDate,
        inputPlaceholder: 'Select a date',
        showCancelButton: true,
        confirmButtonColor: '#198754',
        showLoaderOnConfirm: true,
        backdrop: true,
        confirmButtonText: 'Yes, sure!',
        preConfirm: async (confirmedDate) => {
            confirmedDate = moment(confirmedDate).format("YYYY-DD-MM")
            return booking.updateStatusBooking(id, {status: "approved", confirmedDate})
                .then(result => {
                    if (!result.success) {
                        Swal.showValidationMessage(
                            `Internal server error, please try again`
                        )
                    }
                    
                    message.success(result.message);
                    handleFetchDataTable(tableParams.keyword);
                    toggleModal('view');
                })
            .catch(error => {
                Swal.showValidationMessage(
                    `Internal server error, please try again`
                )
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
      })

      setLoadingProcess(false);
  }
  

  return (<>
    <Modal
      visible={showModal.view}
      title="Detail Booking"
      onCancel={() => toggleModal('view')}
      footer={footer}
    >
      <Descriptions bordered size='small'>
        <Descriptions.Item label="Event" span={3}>{dataViewEvent.event}</Descriptions.Item>
        <Descriptions.Item label="Company" span={3}>{dataViewEvent.company}</Descriptions.Item>
        <Descriptions.Item label="Vendor" span={3}>{dataViewEvent.vendor}</Descriptions.Item>
        <Descriptions.Item label="Purposed Date" span={3}>
          {dataViewEvent.purposedDate && dataViewEvent.purposedDate.map((row, index) => {
            return (
              <Tag color='red' key={index}>
                {row}
              </Tag>
            );
          })}
        </Descriptions.Item>
        
        {dataViewEvent.status === "approved" && (
          <Descriptions.Item label="Confirmed Date" span={3}>
            {dataViewEvent.confirmedDate && (
              <Tag color='green' key={dataViewEvent.confirmedDate}>
                {moment(dataViewEvent.confirmedDate).format("MM-DD-YYYY")}
              </Tag>
            )}
          </Descriptions.Item>
        )}
        <Descriptions.Item label="Address" span={3}>{dataViewEvent.address}</Descriptions.Item>
        <Descriptions.Item label="Status" span={3}>
          {dataViewEvent.status === "approved" && ( <Tag color="#198754">Approved</Tag> )}
          {dataViewEvent.status === "rejected" && ( <Tag color="#dc3545">Rejected</Tag> )}
          {dataViewEvent.status === "pending" && ( <Tag color="#d9a813">Pending</Tag> )}
        </Descriptions.Item>
        {dataViewEvent.status=== "rejected" && (
          <Descriptions.Item label="Remark" span={3}>
            {dataViewEvent.remark}
          </Descriptions.Item>
        )}
      </Descriptions>
    </Modal>
  </>)
}

export default BookingModalEdit