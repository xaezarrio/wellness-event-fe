import { Button, Row, Table, Col, Tag, Select } from 'antd'
import React, { useEffect, useState, useContext } from 'react'
import { BookingContext } from './BookingContext';
import moment from "moment";
import { ROLE } from "../../helper/Constant";

function BookingTable() {
  const Option = Select;

  const {
    state: {
      tableParams
    },
    data: {
      userData,
      dataBooking,
      dataEvent,
    },
    action: {
      toggleModal,
      handleFetchDataTable,
      setTableParams,
      handleViewBooking,
      fetchData
    }
  } = useContext(BookingContext)
  const [loading, setLoading] = useState(false)

  const column = [
    {
      title: '#',
      dataIndex: '_id',
      key: 'no',
      render: (text, record, index) => <span>{index+1}</span> 
    },
    {
      title: 'Event',
      dataIndex: ["event", "name"],
      key: 'event'
    },
    {
      title: 'Vendor',
      dataIndex: ["vendor", "name"],
      key: 'vendor',
    },
    {
      title: 'Confirmed Date',
      dataIndex: 'purposedDate',
      key: 'purposedDate',
      render: (text, record) => (
        <>
          {record.confirmedDate ? (<>
            <Tag color='green' key={record.confirmedDate}>
              {moment(record.confirmedDate).format("DD-MM-YYYY")}
            </Tag>
          </>) : (<>
            {text.map((item, index) => {
              return (
                <Tag color='red' key={index}>
                  {moment(item).format("DD-MM-YYYY")}
                </Tag>
              );
            })}
          </>)}
        </>
      ),
    },
    {
      title: 'Location',
      dataIndex: 'locationAddress',
      key: 'locationAddress',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text) => (<>
            {text === "approved" && ( <Tag color="#198754">Approved</Tag> )}
            {text === "rejected" && ( <Tag color="#dc3545">Rejected</Tag> )}
            {text === "pending" && ( <Tag color="#d9a813">Pending</Tag> )}
      </>)
    },
    {
      title: 'action',
      key: 'action',
      render: (text, record) => (<>
        <Button onClick={() => handleViewBooking(record._id)}>View</Button>
      </>)
    },
  ];


  const handleFetch = async () => {
    setLoading(true);
    await handleFetchDataTable(tableParams.keyword);
    setLoading(false);
  }

  const handleSearch = (keyword) => {
    if (!keyword) keyword = "";
    
    setTableParams('keyword', keyword);
  }

  const handlePagination = (currentPage) => {
    setTableParams('currentPage', currentPage);
  }

  useEffect(() => { handleFetch() }, [tableParams])
  
  return (<>
    <Row>
      <Col span={6}>
        
        <Select
          showSearch
          allowClear
          placeholder="Select an event"
          optionFilterProp="children"
          onChange={handleSearch}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
          style={{
            marginBottom: "15px",
            width: "70%"
          }}
        >
          {dataEvent.map(item => (
            <Option value={item._id} key={item._id}>{item.name}</Option>
          ))}
        </Select>
      </Col>
      <Col 
        span={18}
        style={{
          textAlign: "right"
        }}
      >
        
        {userData.role === ROLE.HR && (
        <Button htmlType="button"
          onClick={() => toggleModal('add')}
        >Add</Button>
        )}
      </Col>
    </Row>
    <Table 
      rowKey="_id"
      loading={loading}
      columns={column}
      dataSource={dataBooking.data}
      pagination={{
        current: tableParams.currentPage,
        total: dataBooking.totalRow,
        pageSize: 10,
        onChange: (current) => {
          handlePagination(current)
        }
      }}
    />
  </>)
}

export default BookingTable