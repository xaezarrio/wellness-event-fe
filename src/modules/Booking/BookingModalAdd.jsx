import { Modal, Form, Select, DatePicker, Button } from 'antd'
import React, { useContext, useState, useEffect } from 'react'
import { BookingContext } from './BookingContext';
import booking from '../../apis/booking'
import moment from 'moment';
import { message } from 'antd'

const BookingModalAdd = () => {

  const { Option } = Select;
  
  const [form] = Form.useForm();

  const {
    state: {
      showModal,
      tableParams
    },
    data: {
      dataEvent,
      dataVendor,
      dataPostal,
      dataAddress
    },
    action: {
      toggleModal,
      handleChangePostal,
      loadOptPostalCode,
      handleFetchDataTable
    }
  } = useContext(BookingContext)

  const layout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 27 },
  };

  const [loading, setLoading] = useState(false)

  const onFinish = (values) => {
    setLoading(true);
    const purposedDate = values.purposedDate.map(row => moment(row).format("YYYY-MM-DD"));
    const storeValue = {
      event: values.event,
      locationAddress: values.address,
      locationPostal: values.postal,
      purposedDate: purposedDate,
      vendor: values.vendor,
      status: "pending"
    };

    booking.storeBooking(storeValue)
    .then(() => {
      handleFetchDataTable(tableParams.keyword)
      message.success('Event has been submitted');
      form.resetFields();
      setLoading(false);
      toggleModal('add');
    }).catch(err => {
      console.log(err);
      message.error('Oops! something wrong, please try again');
      setLoading(false);
    })
  };

  
  return (
    <Modal 
      title="Add New Event" 
      visible={showModal.add} 
      onCancel={() => toggleModal('add')}
      okText="Submit"
      footer={[
        <Button key="back" onClick={() => toggleModal('add')}>
          Return
        </Button>,
        <Button form="formAdd" type="primary" loading={loading} key="submit" htmlType="submit">
            Submit
        </Button>
      ]}
    >
      <Form form={form} id="formAdd" {...layout} name="form-add-booking" onFinish={onFinish}>
        <Form.Item label="Event" name="event" required rules={[{ required: true, message: 'Event is required!' }]}>
          <Select
            showSearch
            placeholder="Select an event"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            rules={[
              {
                required: true,
                message: 'Event is required!',
              },
            ]}
          >
            {dataEvent.map(item => (
              <Option value={item._id} key={item._id}>{item.name}</Option>
            ))}
          </Select>
        </Form.Item>
        
        <Form.Item label="Vendor" name="vendor" required rules={[{ required: true, message: 'Vendor is required!' }]}>
          <Select
            showSearch
            placeholder="Select a vendor"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            rules={[
              {
                required: true,
                message: 'Vendor is required!',
              },
            ]}
          >
            {dataVendor.map(item => (
              <Option value={item._id} key={item._id}>{item.name}</Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name={["purposedDate", 0]} label="Purposed Date 1" required rules={[{ required: true, message: 'Purposed Date 1 is required!' }]}>
          <DatePicker 
            rules={[
              {
                required: true,
                message: 'Purposed Date 1 is required!',
              },
            ]} />
        </Form.Item>
        <Form.Item name={["purposedDate", 1]} label="Purposed Date 2" required rules={[{ required: true, message: 'Purposed Date 2 is required!' }]}>
          <DatePicker 
            rules={[
              {
                required: true,
                message: 'Purposed Date 2 is required!',
              },
            ]} />
        </Form.Item>
        <Form.Item name={["purposedDate", 2]} label="Purposed Date 3" required rules={[{ required: true, message: 'Purposed Date 3 is required!' }]}>
          <DatePicker 
            rules={[
              {
                required: true,
                message: 'Purposed Date 3 is required!',
              },
            ]} />
        </Form.Item>
        <Form.Item label="Postal Code" name="postal" required rules={[{ required: true, message: 'Postal Code is required!' }]}>
          <Select
          showSearch
          placeholder="Select a postal code"
          defaultActiveFirstOption={false}
          filterOption={false}
          onSearch={loadOptPostalCode}
          onChange={value =>  handleChangePostal(value)}
          notFoundContent={null}
          style={{ width: 150 }}
          rules={[
            {
              required: true,
              message: 'Postal Code is required!',
            },
          ]}
        >
          
          {dataPostal.map(item => (
              <Option value={item._id} key={item._id}>{item._id}</Option>
          ))}
        </Select>
        </Form.Item>
        <Form.Item label="Address" name="address" required rules={[{ required: true, message: 'Address is required!' }]}>
          <Select
            showSearch
            placeholder="Select an address"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            style={{ width: 330 }}
          >
            {dataAddress.map(item => (
              <Option value={item.ADDRESS} key={item._id}>{item.ADDRESS}</Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    
    </Modal>
  )
}

export default BookingModalAdd