import React, { createContext, useState, useEffect } from 'react'
import { message } from 'antd'
import event from '../../apis/event'
import vendor from '../../apis/vendor'
import postalcode from '../../apis/postalcode'
import booking from '../../apis/booking'
import moment from 'moment'
import { userData } from "../../helper/Common";

const initialValues = {
  setState: () => {},
  state: {
    showModal: {
      add: false,
      view: false
    },
    tableParams: {
      keyword: "",
      currentPage: 1
    },
    dataViewEvent: {}
  },
  data: {
    userData: userData(),
    dataBooking: {
      data: [],
      totalRow: 0
    },
    dataEvent: [],
    dataVendor: [],
    dataPostal: [],
    dataAddress: [],
    dataViewEvent: {},
  },
  action: {}
};
const BookingContext = createContext(initialValues);

const useBookingContext = () => {
  const [state, setState] = useState(initialValues.state);
  const [data, setEnvironmentData] = useState(initialValues.data);

  const toggleModal = (modalName) => {
    setState(prev => ({
      ...prev,
      showModal: {
        ...prev.showModal,
        [modalName]: !prev.showModal[modalName]
      }
    }));
  }

  useEffect(() => { fetchData() }, []);

  //---- BookingTable Func
  
  const setTableParams = (key, value) => {
    setState(prev => ({
      ...prev,
      tableParams: {
        ...prev.tableParams,
        [key]: value
      }
    }));
  }

  const handleFetchDataTable = async (keyword) => {
    await booking.getAllBookings(keyword)
      .then(result => {
        setEnvironmentData(prev => ({
          ...prev,
          dataBooking: result
        }));
      }).catch(err => {
        console.log(err);
        message.error('Fetch data failed, please try again');
      })
  }

  const handleViewBooking = async (id) => {
    toggleModal('view');
    await booking.getBookingById(id)
    .then((result ) => {
        let bookingData = result.data;
        let purposedDate = bookingData.purposedDate.map(item => {
            return moment(item).format("DD-MM-YYYY");
        })

        let confirmedDate = bookingData.confirmedDate ? 
            moment(bookingData.confirmedDate).format("DD-MM-YYYY") : "";
        const dataViewEvent = {
            id,
            event: bookingData.event.name,
            company: bookingData.company.name,
            vendor: bookingData.vendor.name,
            purposedDate: purposedDate,
            confirmedDate: confirmedDate,
            postal: bookingData.locationPostal,
            address: bookingData.locationAddress,
            status: bookingData.status,
            remark: bookingData.remark
        };

        setEnvironmentData(prev => ({
          ...prev,
          dataViewEvent
        }));
    }).catch(err => {
      console.log(err);
      message.error('Gagal mengambil data event');
    })
  }

  // BookingTable Func ----
  
  //---- BookingModalAdd Func

  const fetchData = async () => {
    try {
      const fetchEvent = await event.getAllEvent();
      const fetchVendor = await vendor.getAllVendor();
      setEnvironmentData(prev => ({
        ...prev,
        dataEvent: fetchEvent.data,
        dataVendor: fetchVendor.data
      }));
    } catch (error) {
      message.error('Fetch data failed, please try again');
    }
  }

  const loadOptPostalCode = async (inputValue) => 
    new Promise((resolve, reject) => {
      if (inputValue) {
          postalcode.getAllPostal(inputValue)
          .then(result => {
              resolve(
                setEnvironmentData(prev => ({
                  ...prev,
                  dataPostal: result.data,
                }))
              );
          }).catch(err => {
              console.log(err);
              reject([])
          })
      } else {
          reject([])
      }
  });

  const handleChangePostal = async (inputValue) => {
    try {
      const fetchAddress = await postalcode.getAddressByPostal(inputValue);
      setEnvironmentData(prev => ({
        ...prev,
        dataAddress: fetchAddress.data,
      }));
    } catch (error) {
      console.log(error);
      message.error('Fetch data failed, please try again');
    }
  }
  // BookingModalAdd Func ----

  return {
    state,
    setState,
    data,
    action: {
      toggleModal,
      handleFetchDataTable,
      setTableParams,
      handleViewBooking,
      handleChangePostal,
      loadOptPostalCode,
      fetchData
    }
  }
}

const BookingProvider = props => {
  const { Provider } = BookingContext
  return <Provider value={useBookingContext()}>{props.children}</Provider>
}
export { BookingContext, BookingProvider }