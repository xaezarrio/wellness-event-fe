import jwt_decode from 'jwt-decode'

const userData = () => {
    const token = localStorage.getItem('userToken')
    let User = ''
  
    if (token) {
      User = jwt_decode(token)
    }
    return User
  }

export {
    userData
}