import Login from "./pages/Login";  
import Booking from "./pages/Booking";  

const routes = [
    {
        path: '/',
        exact: true,
        name: 'Booking',
        component: Booking,
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
    },
]

export default routes