# WELLNESS EVENT APPS

Frontend for WELLNESS EVENT APPS

## AWS
* [13.213.29.230](http://13.213.29.230:3000/)

## Tech Stack

* **Build** : React.js
* **Library** : Antd, Axios, Jwt Decode, Moment, Redux, Sweetalert

## Requirements

* [node.js](https://www.npmjs.com/get-npm)

## Usage

1. Clone this repo.
2. configure .env with .env.example
3. run `npm install` to install all package.
4. run `npm start` to run app.
5. open browsers and open `localhost:3000`, it should go to **Login Page**